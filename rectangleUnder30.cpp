
#include <iostream>
#include <cmath>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>

using namespace std;

int findSingle(int n1, int n2) {
    return abs(n1 - n2);
}


int process(int a, int b, int c, int d, int current=0) {
//    printf("%d, %d, %d, %d\n", a, b, c, d);
    if ((a == 0) && (b == 0) && (c == 0) && (d == 0)) {
        return current;
    }
    return process(findSingle(a, b), findSingle(b, c), findSingle(c, d), findSingle (d, a), current+1);
}

int main() {
    int max = 0;
    vector<pair<int, string>> save;
    for (int i = 30; i > 0; i--) {
        for (int j = 30; j > 0; j--) {
            for (int k = 30; k >0; k--) {
                for (int l = 30; l > 0; l--) {
                    int layers = process(i, j, k, l);
                    stringstream ss;
                    ss << "(" << i << ", " << j << ", " << k << ", " << l << ")";
                    save.emplace_back(pair(layers, ss.str()));
                    layers > max ? max = layers : max = max;
                }
            }
        }
    }
    sort(save.begin(), save.end());

    ofstream file;
    file.open("save.txt");

    for (auto const& [layers, config] : save) {
        file << layers << " : " << config << endl;
    }
    file.close();
}

